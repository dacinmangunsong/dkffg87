#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=daggerhashimoto.usa.nicehash.com:3353
WALLET=32yAg5MbySmKasUBeTL12BCeiAdFeNvnN1.arny

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
done
